﻿from knox import views as knox_views
from .views import StatusAPI, LoginAPI, studentApi,UserSerializer
from django.urls import path
from accounts import views

urlpatterns = [
    
    path('student/login/<str:Username>/<str:Password>/', studentApi, name='studentApi'),
  
    path('login/', views.StatusApi, name='Application Status'),
    
]

