from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


class Status(models.Model):
    user_name = models.CharField(max_length=200)
    job_title = models.CharField(max_length=200)
    application_status = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200)
    salary = models.FloatField()
    Applicationstage = models.CharField(max_length=200)


from hireIN_apps.userprofile.models import Status
class Status(models.Model):

    FAILED = 'failed'
    PENDING = 'pending'
    SUCCESSFUL = 'successful'
    Application_stage = (
        (FAILED, 'failed'),
        (PENDING, 'pending')
        (SUCCESSFUL, 'successful')
    )
    user_name = models.CharField(max_length=200)
    job_title = models.CharField(max_length=200)
    application_status = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200)
    location = models.CharField(max_length=255, blank=True, null=True)
    salary = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=20, choices=Application_stage, default=PENDING)
   
    Application_stage=models.ForeignKey(Status, related_name='Applicationstage', on_delete=models.CASCADE, null=True)   
    created_by = models.ForeignKey(Employer, related_name='Employer_id', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    changed_at = models.DateTimeField(auto_now=True)




