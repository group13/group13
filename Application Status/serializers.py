#serializers.py
from rest_framework import serializers
from hireIN_apps.job.models import Status

class StuatusSerializer(serializers.ModelSerializer):
    user_name = models.CharField(max_length=200)
    job_title = models.CharField(max_length=200)
    application_status = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200)
    salary = models.FloatField()
    
  
    class Meta:
        model = Status
        fields = ('user_name', 'job_title', 'application_status', 'location', 'company_name','salary')

