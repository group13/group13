
from django.db import models

class Student(models.Model):
    id = models.AutoField(primary_key=True)
    Username = models.CharField(max_length = 20, null=True, default='SOME STRING')
    FirstName = models.CharField(max_length = 20, null=True, default='SOME STRING')
    LastName = models.CharField(max_length = 20, null=True, default='SOME STRING')
    Email = models.CharField(max_length = 80, null=True, default='SOME STRING')
    Password = models.CharField(max_length = 20, null=True, default='SOME STRING')
    Location = models.CharField(max_length = 20, null=True, default='SOME STRING')
    VisaStatus = models.CharField(max_length = 20, null=True, default='SOME STRING')
    def __str__(self):
        return self.Username

class Employer(models.Model):
    id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length = 80, null=True, default='SOME STRING')
    Email = models.CharField(max_length = 80, null=True, default='SOME STRING')
    Password = models.CharField(max_length = 20, null=True, default='SOME STRING')
    Website = models.CharField(max_length = 80, null=True, default='SOME STRING')
    Location = models.CharField(max_length = 80, null=True, default='SOME STRING')
    Role = models.CharField(max_length = 80, null=True, default='SOME STRING')
    Size = models.CharField(max_length = 20, null=True, default='SOME STRING')
    Industry = models.CharField(max_length = 80, null=True, default='SOME STRING')
    Funding_level = models.CharField(max_length = 20, null=True, default='SOME STRING')
    def __str__(self):
        return self.Name

class Company(models.Model):
    id = models.AutoField(primary_key=True)
    company_name = models.CharField(max_length = 80, null=True, default='SOME STRING')
