from django.urls import path, include

from .views import view_application, my_jobs, my_applications, list_applications
# from hireIN_apps.job.views import job_application_edit
urlpatterns = [
    path('application/<int:application_id>/', view_application, name='view_application'),
    path('myjobs/<int:employer__id>/', my_jobs, name='myjobs'),
    path('myapplications/<int:student__id>/', my_applications, name='my_applications'),
    path('listapplications/<int:employer__id>/', list_applications, name='list_applications'),
]
