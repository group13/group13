from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from hireIN_apps.job.models import Job, Application
from hireIN_apps.job.serializers import JobSerializer, ApplicationSerializer

@api_view(['GET'])
def my_jobs(request, employer__id):
    if request.method == 'GET':
        data = Job.objects.filter(employer__id = employer__id)
        serializer = JobSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def my_applications(request, student__id):
    if request.method == 'GET':
        data = Application.objects.filter(student__id = student__id)
        serializer = ApplicationSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def list_applications(request, employer__id):
    if request.method == 'GET':
        data = Application.objects.filter(employer__id = employer__id)
        serializer = ApplicationSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def view_application(request, application_id):
    if request.method == 'GET':
        data = Application.objects.filter(pk=application_id)
        serializer = ApplicationSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)
