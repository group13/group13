from django.contrib import admin

from .models import Student, Employer

admin.site.register(Student)
admin.site.register(Employer)
