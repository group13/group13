# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from hireIN_apps.accounts.models import Student, Employer
class Job(models.Model):

    # fields = [ 'job_title', 'description', 'company_name', 'location', 'deadline', 'job_category', 'salary']
    ACTIVE = 'active'
    EMPLOYED = 'employed'
    CHOICES_STATUS = (
        (ACTIVE, 'Active'),
        (EMPLOYED, 'Employed')
    )

    job_title = models.CharField(max_length=255)
    description = models.TextField()
    company_name = models.CharField(max_length=255)
    location = models.CharField(max_length=255, blank=True, null=True)
    deadline = models.CharField(max_length=255, blank=True, null=True)
    job_category = models.CharField(max_length=255, blank=True, null=True)
    salary = models.CharField(max_length=255, blank=True, null=True)

    employer = models.ForeignKey(Employer, related_name='job_employer_id', on_delete=models.CASCADE)
    created_by = models.ForeignKey(Employer, related_name='jobs', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    changed_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=20, choices=CHOICES_STATUS, default=ACTIVE)
    def __str__(self):
        return self.job_title

class Application(models.Model):
    FAILED = 'failed'
    PENDING = 'pending'
    SUCCESSFUL = 'successful'
    STATUS_CHOICES = (
        (FAILED, 'failed'),
        (PENDING, 'pending'),
        (SUCCESSFUL, 'successful')
    )
    app_status = models.CharField(max_length=20, choices=STATUS_CHOICES, default=PENDING)
    job = models.ForeignKey(Job, related_name='applications', on_delete=models.CASCADE)
    student = models.ForeignKey(Student, related_name='student_id', on_delete=models.CASCADE, null=True)
    employer = models.ForeignKey(Employer, related_name='application_employer_id', on_delete=models.CASCADE)
    interest = models.TextField()
    experience = models.TextField()
    # application_status = models.ForeignKey(Status, related_name='application_status', on_delete=models.CASCADE, null=True)
    created_by = models.ForeignKey(Student, related_name='applications', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
