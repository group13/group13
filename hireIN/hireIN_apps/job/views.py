from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from rest_framework.decorators import api_view
from .models import Job, Application
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics, permissions, viewsets, status
from .serializers import JobSerializer, ApplicationSerializer, StatusSerializer
from rest_framework.views import APIView

class JobList(generics.ListAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

class JobDelete(generics.RetrieveDestroyAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

class JobEdit(generics.RetrieveUpdateAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    lookup_field = 'pk'

class FullApplicationList(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer

class ApplicationEdit(generics.RetrieveUpdateAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer
    lookup_field = 'pk'

class ApplicationDelete(generics.RetrieveDestroyAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer

class create_job(generics.CreateAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

@api_view(['GET'])
def job_detail(request, job_id):
    if request.method == 'GET':
        data = Job.objects.filter(pk=job_id)
        serializer = JobSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)

@api_view(['GET', 'POST'])
def apply_for_job(request, job_id):
    if request.method == 'GET':
        data = Job.objects.filter(pk=job_id)
        serializer = JobSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ApplicationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def view_application(request, application_id):
    if request.method == 'GET':
        data = Application.objects.filter(pk=application_id)
        serializer = ApplicationSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)

class application_decision(generics.RetrieveUpdateAPIView):
    queryset = Application.objects.all()
    serializer_class = StatusSerializer
    lookup_field = 'pk'

class view_application_decision(generics.ListAPIView):
    queryset = Application.objects.all()
    serializer_class = StatusSerializer
    lookup_field = 'pk'
