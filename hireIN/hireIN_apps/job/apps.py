from django.apps import AppConfig


class JobConfig(AppConfig):
    name = 'hireIN_apps.job'
