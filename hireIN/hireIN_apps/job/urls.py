from django.urls import path, include


from .views import JobDelete, JobList, create_job,  JobEdit, FullApplicationList, ApplicationDelete, ApplicationEdit, apply_for_job, view_application, job_detail, application_decision, view_application_decision
# JobEdit, JobApply, create_job, , edit_job, job_application,

urlpatterns = [
    path('<int:pk>/delete', JobDelete.as_view(), name='jobdelete'),
    path('<int:job_id>/', job_detail, name='job_detail'),
    path('', JobList.as_view(), name='joblist'),
    path('create/', create_job.as_view(), name='create_job'),
    path('<int:pk>/edit/', JobEdit.as_view(), name='edit_job'),
    path('<int:job_id>/job_apply/', apply_for_job, name='job_apply'),
    path('applications/', FullApplicationList.as_view(), name='job_application_view'),
    path('applications/<int:application_id>/', view_application, name='application_detail'),
    path('applications/<int:pk>/delete/', ApplicationDelete.as_view(), name='delete_application'),
    path('applications/<int:pk>/edit/', ApplicationEdit.as_view(), name='edit_application'),
    path('applications/<int:pk>/decision/', application_decision.as_view(), name='application_decision'),
    path('applications/<int:pk>/view_decision/', view_application_decision.as_view(), name='view_application_decision'),

]
