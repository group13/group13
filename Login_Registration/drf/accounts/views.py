from django.shortcuts import render, redirect
from rest_framework import generics, permissions
from rest_framework.response import Response
from knox.models import AuthToken
from .serializers import UserSerializer, RegisterSerializer, EmployerRegisterSerializer
from rest_framework.decorators import api_view, permission_classes

from django.contrib.auth import login

from rest_framework import permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView


from .models import Student, Employer
from django.http.response import JsonResponse
from rest_framework.permissions import IsAuthenticated

ERROR_REPONSE = "error"

class UserSerializer(generics.GenericAPIView):
    serializer_class = UserSerializer
    permission_classes = (permissions.AllowAny,)

def studentApi(request, Username, Password):
    permission_classes = (permissions.AllowAny,)
    permission_classes = [IsAuthenticated]
    if request.method == 'GET':
        # receive your POST data here
        #Student_id = Username
        if not Student.objects.filter( Username=Username, Password =Password).exists():
            #Votes.objects.create(**your_data)
            return JsonResponse(ERROR_REPONSE,safe=False)
        #redirect('your-desired-url')
        #return render(request, 'test.html', {'student': Student})

        data = str(Student.objects.filter( Username=Username, Password =Password).values_list('id'))
        tempData = (data.split('(')[1])
        id = int(tempData.split(',')[0])

        return JsonResponse(id,safe=False)
    else:
        return JsonResponse(ERROR_REPONSE,safe=False)

def employerApi(request, Email, Password):
    permission_classes = (permissions.AllowAny,)
    permission_classes = [IsAuthenticated]
    if request.method == 'GET':
        # receive your POST data here
        #Student_id = Username
        if not Employer.objects.filter( Email=Email, Password =Password).exists():
            #Votes.objects.create(**your_data)
            return JsonResponse(ERROR_REPONSE,safe=False)
        #redirect('your-desired-url')
        #return render(request, 'test.html', {'student': Student})
        data2 = str(Employer.objects.filter( Email=Email, Password =Password).values_list('id'))
        tempData2 = (data2.split('(')[1])
        employerid = int(tempData2.split(',')[0])

        # testing sending company_id

        return JsonResponse(employerid,safe=False)
    else:
        return JsonResponse(ERROR_REPONSE,safe=False)


# Register API
class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer
    permission_classes = (permissions.AllowAny,)

#WORKING POST
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        #user = serializer.save()
        try:
            user = serializer.save()
            return JsonResponse("Registered",safe=False)
        except:
            return JsonResponse("This username has already been registered",safe=False)
        #return Response({
        #"user": UserSerializer(Student, context=self.get_serializer_context()).data,
        #})


class EmployerRegisterAPI(generics.GenericAPIView):
    serializer_class = EmployerRegisterSerializer
    permission_classes = (permissions.AllowAny,)

#WORKING POST
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        #user = serializer.save()
        try:
            user = serializer.save()
            return JsonResponse("Registered",safe=False)
        except:
            return JsonResponse("This Email has already been registered",safe=False)

        #return JsonResponse("Registered",safe=False)
        #return Response({
        #"user": UserSerializer(Student, context=self.get_serializer_context()).data,
        #})


class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['Student']
        login(request, Student)
        return super(LoginAPI, self).post(request, format=None)
