# Generated by Django 4.0.3 on 2022-04-21 17:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_company_employer_role_employer_company_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='employer',
            old_name='Company_id',
            new_name='Company',
        ),
    ]
