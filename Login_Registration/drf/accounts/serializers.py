from rest_framework import serializers
from django.contrib.auth.models import User

from .models import Student, Employer, Company
from django.http.response import JsonResponse

# User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        #fields = ('id', 'username', 'email')
        #Alan change
        fields = ('Username','FirstName','LastName','Email', 'Password', 'Location', 'VisaStatus')


# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        #fields = ('id', 'username', 'Email', 'Password', 'Location', 'VisaStatus', 'RolePreference', 'LevelOfTheRole', 'CompanySizePreference', 'IndustryPreference', 'TechnologyPreference', 'MinimumSalaryException')
        fields = ('Username','FirstName','LastName','Email', 'Password', 'Location', 'VisaStatus')

        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        try:
            obj = Student.objects.get(Username = validated_data['Username'])
        except Student.DoesNotExist:
            return Student.objects.create(VisaStatus = validated_data['VisaStatus'], Username = validated_data['Username'],FirstName = validated_data['FirstName'], LastName = validated_data['LastName'], Email = validated_data['Email'], Password = validated_data['Password'], Location = validated_data['Location'])


class EmployerRegisterSerializer(serializers.ModelSerializer):

    #Company = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all())
    #Company = serializers.PrimaryKeyRelatedField(queryset=Company.objects.get(Company_id = '1'))



    class Meta:
        model = Employer
        fields = ('Name','Email','Password','Role','Size','Industry','Website','Location','Funding_level',)
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        try:
            obj = Employer.objects.get(Email = validated_data['Email'])
        except Employer.DoesNotExist:
            #Company = serializers.PrimaryKeyRelatedField(queryset=Company.objects.get(Company_id = validated_data['Company']))
            return Employer.objects.create( Name = validated_data['Name'], Email = validated_data['Email'], Password = validated_data['Password'], Role = validated_data['Role'], Size = validated_data['Size'], Industry = validated_data['Industry'], Website = validated_data['Website'], Location = validated_data['Location'], Funding_level = validated_data['Funding_level'])

        #return user
