from knox import views as knox_views
from .views import RegisterAPI, LoginAPI, studentApi,UserSerializer, EmployerRegisterAPI, employerApi
from django.urls import path
from accounts import views

urlpatterns = [
    path('student/register/', RegisterAPI.as_view(), name='register'),
    path('employer/register/', EmployerRegisterAPI.as_view(), name='employerregister'),


    path('student/login/<str:Username>/<str:Password>/', studentApi, name='studentApi'),
    path('employer/login/<str:Email>/<str:Password>/', employerApi, name='employerApi'),


    path('login/', views.studentApi, name='login'),
    path('logout/', knox_views.LogoutView.as_view(), name='logout'),
    path('logoutall/', knox_views.LogoutAllView.as_view(), name='logoutall'),
    #path('login/', studentApi, name='studentApi'),
]
