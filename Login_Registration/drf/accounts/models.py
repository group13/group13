from django.db import models

DEFAULT_COMPANY_ID = 1
# Create your models here.
class Company(models.Model):
    Company_id = models.AutoField(primary_key=True)
    Company_name = models.CharField(max_length = 80, null=True, default='SOME STRING')
#    class Meta:
#      db_table = 'Company'

class Student(models.Model):
    Username = models.CharField(max_length = 20, null=True, default='Empty')
    id = models.AutoField(primary_key=True)
    FirstName = models.CharField(max_length = 20, null=True, default='Empty')
    LastName = models.CharField(max_length = 20, null=True, default='Empty')
    Email = models.CharField(max_length = 80, null=True, default='Empty')
    Password = models.CharField(max_length = 20, null=True, default='Empty')
    Location = models.CharField(max_length = 20, null=True, default='Empty')
    VisaStatus = models.CharField(max_length = 20, null=True, default='Empty')
    #RolePreference = models.CharField(max_length = 20, null=True, default='SOME STRING')
    #LevelOfTheRole = models.CharField(max_length = 20, null=True, default='SOME STRING')
    #CompanySizePreference = models.CharField(max_length = 20, null=True, default='SOME STRING')
    #IndustryPreference = models.CharField(max_length = 20, null=True, default='SOME STRING')
    #TechnologyPreference = models.CharField(max_length = 20, null=True, default='SOME STRING')
    #MinimumSalaryException = models.CharField(max_length = 20, null=True, default='SOME STRING')
#    class Meta:
#      db_table = 'Student'


class Employer(models.Model):
    id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length = 80, null=True, default='Empty')
    Email = models.CharField(max_length = 80, null=True, default='Empty')
    Password = models.CharField(max_length = 20, null=True, default='Empty')
    Role = models.CharField(max_length = 20, null=True, default='Empty')
    Size = models.CharField(max_length = 80, null=True, default='Empty')
    Industry = models.CharField(max_length = 80, null=True, default='Empty')
    Website = models.CharField(max_length = 80, null=True, default='Empty')
    Location = models.CharField(max_length = 80, null=True, default='Empty')
    Funding_level = models.CharField(max_length = 80, null=True, default='Empty')

#    class Meta:
#      db_table = 'Employer'







#    Student_id INT NOT NULL,
#    FirstName VARCHAR(255) NOT NULL,
#    LastName VARCHAR(255) NOT NULL,
#    Email VARCHAR(255) NOT NULL,
#    Password VARCHAR(255) NOT NULL,
#    Location VARCHAR(255) NOT NULL,
#    VisaStatus VARCHAR(255) NOT NULL,
#    RolePreference VARCHAR(255) NOT NULL,
#    LevelOfTheRole VARCHAR(255) NOT NULL,
#    CompanySizePreference VARCHAR(255) NOT NULL,
#    IndustryPreference VARCHAR(255) NOT NULL,
#    TechnologyPreference VARCHAR(255) NOT NULL,
#    MinimumSalaryException INT NOT NULL,
#    PRIMARY KEY (Student_id)
