import axios from "axios";
import API from "../apis/config";
import React, { useState } from 'react';


//-----------GET LIST OF JOB ------------
const getListOfJobs = async() =>
{
  const url = `${API.STUDENT_URL_BASE}/api/`;
  console.log(url);
  return fetch(`${API.STUDENT_URL_BASE}/api/`)
    .then((response) =>
    {
      console.log("first response", response.data);
      const feedback = response.json();
      return (feedback);
    })
    .catch(error => console.log(error));
};

//-----------GET A SPECIFIC JOB ------------
const getSpecificJobs = async() =>
{

  var id = sessionStorage.getItem("job_id");
  const url = `${API.STUDENT_URL_BASE}/api/${id}/job_apply/`;

  return fetch(url)
    .then((response) =>
    {

      console.log("first response", response.data);

      const feedback = response.json();

      return (feedback);

    })
    .catch(error => console.log(error));
};

//-------------APPLY FOR A JOB----------------------//
const applyForJob = async(interest, experience, createdId, employerId, jobId, studentId) =>
{
  /* //---- CONSOLE LOG FOR TESTING -------
  console.log("interest app", interest)
  console.log("experince app", experience)
  console.log("created by", createdId)
  console.log("eployer Id", employerId)
  console.log("job Id", jobId)

  */
  const url =  `${API.STUDENT_URL_BASE}/api/${jobId}/job_apply/`;
  return axios
    .post(url,
      {
        interest: interest,
        experience: experience,
        created_by:  createdId,
        student:  createdId,
        employer: employerId,
        job:  jobId,
      })
    .then((response) => {
      console.log(response);
      return response.data;
    });
};

const jobService =
{
  getListOfJobs,
  getSpecificJobs,
  applyForJob,
};

export default jobService;
