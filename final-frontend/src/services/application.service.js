import axios from "axios";
import API from "../apis/config";

//GET STUDENT APPLICATIONS
const getStudentApplication = async(studentId) =>
{
  const url = `${API.STUDENT_URL_BASE}/dashboard/myapplications/${studentId}/`;
  return fetch(url)
    .then((response) =>
    {
      console.log("first response", response.data);
      const feedback = response.json();
      return (feedback);
    })
    .catch(error => console.log(error));
}

//VIEW AN APPLICATIONS
const viewStudentApplication = async(applicationId) =>
{
  const url = `${API.STUDENT_URL_BASE}/api/applications/${applicationId}/`;
  console.log(url)
  return fetch(url)
    .then((response) =>
    {
      const feedback = response.json();
      if (feedback == []){
        return (["nothing"])
      }
      else{
          console.log(feedback);
      return (feedback);
    }

    })
    .catch(error => console.log(error));
}

//EDIT APPLICATIONS
const editStudentApplication = async(app_id, job_id, employer, created_by, experience, interest) =>
{
  const url = `${API.STUDENT_URL_BASE}/api/applications/${app_id}/edit/`;
  /* //-- CONSOLE LOG'S FOR TESTING ---//
  console.log("employer", employer)
  console.log("job", job_id)
  console.log("created by", created_by)
  console.log("experience", experience)
  */
  axios
    .put(url, {
          interest: interest,
          experience: experience,
          job: job_id,
          employer: employer,
          created_by: created_by
        })
    .then((response) => {
          console.log(response.data);
        })
}

//DELETE APPLICATIONS
const deleteStudentApplication = async(app_id) =>
{
  const url = `${API.STUDENT_URL_BASE}api/applications/${app_id}/delete/`;
  axios
    .delete(url)
    .then(() => console.log("deleted"));
}


const applicationService =
{
  getStudentApplication,
  viewStudentApplication,
  editStudentApplication,
  deleteStudentApplication,
};


export default applicationService;
