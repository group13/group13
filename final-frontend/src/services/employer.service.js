import axios from "axios";
import API from "../apis/config";
import React, { useState } from 'react';


//-----------GET LIST OF JOB ------------
const getEmployerPostedJobs = async(employerId) =>
{
  const url = `${API.EMPLOYER_URL_BASE}/dashboard/myjobs/${employerId}/`;
  return fetch(url)
    .then((response) =>
    {
      console.log("first response", response.data);
      const feedback = response.json();
      return (feedback);
    })
    .catch(error => console.log(error));
};

//-----------GET A SPECIFIC JOB ------------
const getEmployerSpecificJob = async(employerId) =>
{
  const url = `${API.STUDENT_URL_BASE}/api/${employerId}/job_apply/`;
  return fetch(url)
    .then((response) =>
    {

      console.log("first response", response.data);

      const feedback = response.json();

      return (feedback);

    })
    .catch(error => console.log(error));
};

//-----------GET A SPECIFIC JOB ------------
const getApplicantsToEmployerJob = async(employerId) =>
{
  const url = `${API.EMPLOYER_URL_BASE}/dashboard/listapplications/${employerId}/`;
  return fetch(url)
    .then((response) =>
    {

      const feedback = response.json();
            console.log("first response", feedback);
      return (feedback);
    })
    .catch(error => console.log(error));
};

const updateApplicantToEmployerJob = async() =>
{

  var id = sessionStorage.getItem("job_id");
  const url = `${API.EMPLOYER_URL_BASE}/api/`;

  return fetch(url)
    .then((response) =>
    {

      console.log("first response", response.data);
      const feedback = response.json();
      return (feedback);
      console.log("feedbacl is", feedback);

    })
    .catch(error => console.log(error));
};

//-------------CREATE A NEW FOR A JOB----------------------//
const createNewJob = async(
  job_tile,
  description,
  company_name,
  salary,
  deadline,
  employer_id) =>
{

  console.log(
  'job_title:',  job_tile,
  'description:',  description,
  'company:',company_name,
    salary,
    deadline,
    employer_id)

  const url = `${API.EMPLOYER_URL_BASE}/api/create/`;
  return axios
    .post(url,
      {
        job_title: job_tile,
        description: description,
        company_name:  company_name,
        employer:  employer_id,
        created_by: employer_id,
        deadline:  deadline,
        salary:  salary,
      }
      )
    .then((response) => {
      console.log(response);
      return response.data;
    });
};


//-------------EDIT POSTED JOB----------------------//
const editEmployerPostedJob = async(job_title, description, company_name, employerId, deadline, salary) =>
{
  const url = `${API.EMPLOYER_URL_BASE}/api/${employerId}/edit/`;
  return axios
    .post(url,
      {
        job_title: job_title,
        description: description,
        company_name:  company_name,
        employer:  employerId,
        created_by: employerId,
        deadline: deadline,
        salary: salary,
      })
    .then((response) => {
      console.log(response);
      return response.data;
    });
};

//-------------APPLY FOR A JOB----------------------//
const deleteEmployerPostedJob = async(
  interest, experience, createdId, employerId, jobId, studentId
) =>
{

  const url = `${API.EMPLOYER_URL_BASE}/api/`;
  return axios
    .post(url,
      {
        interest: interest,
        experience: experience,
        created_by:  createdId,
        student:  createdId,
        employer: employerId,
        job:  jobId,
      })
    .then((response) => {
      console.log(response);
      return response.data;
    });
};


const employerService =
{
  createNewJob,
  getEmployerPostedJobs,
  editEmployerPostedJob,
  getEmployerSpecificJob,
  getApplicantsToEmployerJob,
};

export default employerService;
