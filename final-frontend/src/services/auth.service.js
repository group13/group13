import axios from "axios";
import API from "../apis/config";



/* ------------------------EMPLOYER AUTHENTICATION SERVICES ----------------------- */
const employerSignup = (props) =>
{
  const url =  `${API.AUTH_URL_BASE}/employer/register/`;
  console.log("value being passed", props);
  axios
    .post(url, {props})
    .then((response) => {
      console.log("response is", response.data);
      if ((response.data) != "Registered") {
        console.log("error");
      }
      return response.data;
    });
};

//EMPLOYER LOGIN
const employerLogin = async(username, password) =>
{
  const url = `${API.AUTH_URL_BASE}/employer/login/${username}/${password}/`;
  fetch(url)
    .then(function(serverPromise){
      serverPromise.json()
        .then(function(j)
        {
          const feedback = j;
          if ((feedback != "error") && (isNaN(feedback) == false))
          {
              sessionStorage.setItem("employerId", feedback);
              sessionStorage.setItem("employer", username);
          }
          else{
            console.log("user not found")
          }
        })
        .catch(function(e){
          console.log(e);
        });
    })
    .catch(function(e){
        console.log(e);
      });
}

/* ------------------------STUDENT AUTHENTICATION SERVICES ----------------------- */
//STUDENT SIGNUP
const studentSignup = (props) =>
{
  console.log("data is", props);
  const url =  `${API.AUTH_URL_BASE}/student/register/`;
  axios
    .post(url, props)
    .then(response => {

      console.log(response)
        if (response.data == "Registered")
          {
        window.location = "/student-login"
      }

    })
    .catch(error => {
      console.log(error)
    })
};

//STUDENT LOGIN (Working)
const studentLogin = async(username, password) =>
{
  const url = `${API.AUTH_URL_BASE}/student/login/${username}/${password}/`;
  fetch(url)
    .then(function(serverPromise){
      serverPromise.json()
        .then(function(j)
        {
          const userId = j;
          console.log("response", j)
          if ((userId != "error") || (isNaN(userId) == false))
          {
              sessionStorage.setItem("userId", userId);
              sessionStorage.setItem("user", username);
          }
          else{
            console.log("user not found")
          }
        })
        .catch(function(e){
          console.log(e);
        });
    })
    .catch(function(e){
        console.log(e);
      });
}

/* ----------------------LOGOUT AND USER SERVICES ----------------------- */
const logoutStudent = () =>{
  sessionStorage.removeItem("user");
  sessionStorage.removeItem("userId");
  sessionStorage.removeItem("job_id");
  sessionStorage.removeItem("application_id");
  sessionStorage.removeItem("employerId");
  sessionStorage.removeItem("employer");
  sessionStorage.removeItem("employer_job_id");
  sessionStorage.setItem("user", null);
  sessionStorage.setItem("userId", null);
  sessionStorage.setItem("job_id", null);
  sessionStorage.setItem("application_id",null);
};

const logoutEmployer = () =>{
  sessionStorage.removeItem("user");
  sessionStorage.removeItem("userId");
  sessionStorage.removeItem("employerId");
  sessionStorage.removeItem("employer");
  sessionStorage.removeItem("employer_job_id");
};

const getCurrentStudent = () => {
  return JSON.parse(sessionStorage.getItem("user"));
};

const getCurrentEmployer = () => {
  return JSON.parse(sessionStorage.getItem("user"));
};

const authService =
{
  employerSignup,
  employerLogin,
  studentSignup,
  studentLogin,
  logoutStudent,
  logoutEmployer,
  getCurrentStudent,
  getCurrentEmployer,
};

export default authService;
