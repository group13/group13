//--------------------------importing page-----------------------------------
//Import dependancies and styles
import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import './styles/App.css';

//--------------------------importing page-----------------------------------
//Import start pages
import Start from './pages/start-page'
//Import authentication pages
import StudentLogin from './pages/authentication/student_login'
import Student_Registration from './pages/authentication/student_register'
import Employer_login from './pages/authentication/employer_login'
import EmployerRegistration from './pages/authentication/employer_register'
import Logout from './pages/authentication/logout'
//Import student pages
import List_of_jobs from './pages/student/list-of-jobs-page'
import Apply_for_job from './pages/student/apply-for-job-page'
import Student_applications from './pages/student/manage-applications-page'
import Edit_applications from './pages/student/edit-application-page'
//Import employer pages
import Create_job from './pages/employer/create-job-page'
import List_of_employer_jobs from './pages/employer/list-of-employer-jobs-page'
import Edit_posted_job from './pages/employer/edit-job-page'
import View_applicants from './pages/employer/view-applicants-page'
import Application_Status from './pages/employer/application-status-page'

//--------------------------routing pages-----------------------------------
function App() {
  return (
    <div>
    <Routes>
      <Route path="/" exact element={ <Start/> } />

      <Route path="/student-login" exact element={ <StudentLogin/> } />
      <Route path="/employer-login" exact element={ <Employer_login/> } />
      <Route path="/student-register" exact element={ <Student_Registration/> } />
      <Route path="/employer-register" exact element={ <EmployerRegistration/> } />
      <Route path="/logout" exact element={ <Logout/> } />

      <Route path="/my-applications" exact element={ <Student_applications/> } />
      <Route path="/list-of-jobs" exact element={ <List_of_jobs/> } />
      <Route path="/apply-for-job" exact element={ <Apply_for_job/> } />
      <Route path="/edit-application" exact element={ <Edit_applications/> } />

      <Route path="/create-job" exact element={ <Create_job/> } />
      <Route path="/posted-jobs" exact element={ <List_of_employer_jobs/> } />
      <Route path="/edit-posted-jobs" exact element={ <Edit_posted_job/> } />
      <Route path="/view-applicants" exact element={ <View_applicants/> } />
      <Route path="/update-applicant-status" exact element={ <Application_Status/> } />

    </Routes>
    </div>
  );
}

export default App;
