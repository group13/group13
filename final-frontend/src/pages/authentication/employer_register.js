//IMPORTING FUNCTIONALITIES
import React, { Component} from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';
//IMPORTING DESIGNS
import styled from "styled-components";
import StyledButton from "../../styles/Button"
import "./styles.css"
//IMPORT for REGITSER API
import API from "../../apis/config";
import AuthService from "../../services/auth.service";

class EmployerRegistration extends Component {
  constructor(props) {
    super(props)
    this.state = {
        Name:'',
        Email: '',
        Password: '',
        Role: '',
        Size: '',
        Industry:'',
        Website:'',
        Location:'',
        Funding_level:'',
        }
    }

  //update states
  async changeHandler(e)
  {
  await this.setState(
      {
        [e.target.name]: e.target.value
      })
      console.log(e.target.name)
      console.log(e.target.value)
  }


submitHandler = (e) => {
  e.preventDefault()

  /*
  const name  = this.state.Name;
  const email  = this.state.Email;
  const password = this.state.Password;
  const role = this.state.Role;
  const size = this.state.Size;
  const industry = this.state.Industry;
  const website = this.state.Websitel;
  const location = this.state.Location;
  const funding = this.state.Funding_level;
  */

  AuthService.employerSignup(this.state)
  console.log(this.state)

}

  render() {
   const{username, email, password} = this.state
    const MainContainer = (styled.div`
      display: flex;
      align-items: center;
      flex-direction: column;
      height: 80vh;
      width: 30vw;
      background: rgba(255, 255, 255, 0.15);
      box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
      backdrop-filter: blur(8.5px);
      -webkit-backdrop-filter: blur(8.5px);
      border-radius: 10px;
      color: #000000;
      text-transform: uppercase;
      letter-spacing: 0.4rem;
      @media only screen and (max-width: 320px) {
        width: 80vw;
        height: 90vh;
        hr {
          margin-bottom: 0.3rem;
        }
        h4 {
          font-size: small;
        }
      }
      @media only screen and (min-width: 360px) {
        width: 80vw;
        height: 90vh;
        h4 {
          font-size: small;
        }
      }
      @media only screen and (min-width: 411px) {
        width: 80vw;
        height: 90vh;
      }

      @media only screen and (min-width: 768px) {
        width: 80vw;
        height: 80vh;
      }
      @media only screen and (min-width: 1024px) {
        width: 70vw;
        height: 50vh;
      }
      @media only screen and (min-width: 1280px) {
        width: 30vw;
        height: 80vh;
      }
    `);

    const WelcomeText = (styled.h2`
      margin: 3rem 0 2rem 0;
    `);

    const InputContainer = (styled.div`
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
      height: 80%;
      width: 100%;
    `);

    const ButtonContainer = (styled.div`
      margin: 1rem 0 2rem 0;
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
    `);

    const ForgotPassword = (styled.h4`
      cursor: pointer;
    `);

    const Spacing = (styled.div`
      display: block;
      margin-bottom: 20px;
        `);


      const Spacing2 = (styled.div`
        display: block;
        margin-bottom: 120px;
        `);


    return (
      <div>
        <h2 align="center">Employer register to HireIn!</h2>


        <form onSubmit = {this.submitHandler} align="center">
              <div>
              <input
                    align="center"
                    type="text"
                    placeholder = "Enter New Email"
                    name = "Email"
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>


              <div>
              <input
                    align="center"
                    type="name"
                    placeholder = "Enter New Name"
                    name = "Name"
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>


              <div>
              <input
                    align="center"
                    type="password"
                    placeholder = "Enter New Password"
                    name = "Password"
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>


              <ForgotPassword><Link to='/'> Employer Login instead?</Link></ForgotPassword>
              <StyledButton type = "submit" content = "Register" align="center"></StyledButton>

        </form>
</div>

 );

}
} export default EmployerRegistration;
