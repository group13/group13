//IMPORTING FUNCTIONALITIES
import React, { Component} from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';

//IMPORTING DESIGNS
import styled from "styled-components";
import StyledButton from "../../styles/Button"
import "./styles.css"
//IMPORT for REGITSER API
import API from "../../apis/config";
import AuthService from "../../services/auth.service";

class Student_Registration extends Component {
  constructor(props){
      super(props)
      this.state = {
          Username: '',
          Email: '',
          Password: '',
          FirstName: '',
          LastName: '',
          Location: '',
          VisaStatus: '',
      }
  }

  //update states
  async changeHandler(e)
  {
    await this.setState({[e.target.name]: e.target.value})
    console.log(e.target.name)
    console.log(e.target.value)
  }

  submitHandler = (e) => {
          e.preventDefault();
           AuthService.studentSignup(this.state)
          //console.log(this.state)
  }

  render() {
   const{username, email, password} = this.state
    const MainContainer = (styled.div`
      display: flex;
      align-items: center;
      flex-direction: column;
      height: 80vh;
      width: 30vw;
      background: rgba(255, 255, 255, 0.15);
      box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
      backdrop-filter: blur(8.5px);
      -webkit-backdrop-filter: blur(8.5px);
      border-radius: 10px;
      color: #000000;
      text-transform: uppercase;
      letter-spacing: 0.4rem;
      @media only screen and (max-width: 320px) {
        width: 80vw;
        height: 90vh;
        hr {
          margin-bottom: 0.3rem;
        }
        h4 {
          font-size: small;
        }
      }
      @media only screen and (min-width: 360px) {
        width: 80vw;
        height: 90vh;
        h4 {
          font-size: small;
        }
      }
      @media only screen and (min-width: 411px) {
        width: 80vw;
        height: 90vh;
      }

      @media only screen and (min-width: 768px) {
        width: 80vw;
        height: 80vh;
      }
      @media only screen and (min-width: 1024px) {
        width: 70vw;
        height: 50vh;
      }
      @media only screen and (min-width: 1280px) {
        width: 30vw;
        height: 80vh;
      }
    `);

    const WelcomeText = (styled.h2`
      margin: 3rem 0 2rem 0;
    `);

    const InputContainer = (styled.div`
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
      height: 80%;
      width: 100%;
    `);

    const ButtonContainer = (styled.div`
      margin: 1rem 0 2rem 0;
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
    `);

    const ForgotPassword = (styled.h4`
      cursor: pointer;
    `);

    const Spacing = (styled.div`
      display: block;
      margin-bottom: 20px;
        `);


      const Spacing2 = (styled.div`
        display: block;
        margin-bottom: 120px;
        `);


    return (
      <div>
        <h2 align="center">Student register to HireIn!</h2>


        <form onSubmit = {this.submitHandler} align="center">
              <div>
              <input
                    align="center"
                    type="text"
                    placeholder = "Email"
                    name = "Email"
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>

              <div>
              <input
                    align="center"
                    type="text"
                    placeholder = "Frist Name"
                    name = "FirstName"
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>



              <div>
              <input
                    align="center"
                    type="text"
                    placeholder = "Last Name"
                    name = "LastName"
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>


              <div>
              <input
                    align="center"
                    type="text"
                    placeholder = "Username"
                    name = "Username"
                    value={username}
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>

              <div>
              <input
                    align="center"
                    type="password"
                    placeholder = "Password"
                    name = "Password"
                    value={password}
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>
              <div>
              <input
                    align="center"
                    type="text"
                    name = "Location"
                    placeholder = "Location"
                    onChange={this.changeHandler.bind(this)}/>
              </div>

              <Spacing/>
              <div>
              <input
                    align="center"
                    type="text"
                    placeholder = "Visa Status"
                    name = "VisaStatus"
                    onChange={this.changeHandler.bind(this)}/>
              </div>
              <Spacing/>

              <ForgotPassword><Link to='/'> Student Login instead?</Link></ForgotPassword>
              <StyledButton type = "submit" content = "Register" align="center"></StyledButton>

        </form>
</div>

 );

}
} export default Student_Registration;
