//IMPORTING FUNCTIONALITIES
import React, {Component, useState} from 'react';
import { Link, useNavigate} from "react-router-dom";
import axios from 'axios';

//IMPORTING DESIGNS
import styled from "styled-components";
import {ToastsContainer,ToastsStore, ToastsContainerPosition,} from "react-toasts";
import StyledButton from "../../styles/Button"
import "./styles.css"

//IMPORT for LOGIN API
import API from "../../apis/config";
import AuthService from "../../services/auth.service";

class StudentLogin extends Component
  {
    constructor(props)
      {
        super(props)
        this.state =
        {
          username: '',
          password: '',
        
        }
      }

    //HANDLE CHANLES TO INPUT IN ENTRY FORM
    async changeHandler(e)
    {
      await this.setState(
          {
            [e.target.name]: e.target.value
          })
    }

    //below method handles the backend check for login
    submitHandler =  async (e) =>
    {
      e.preventDefault()
      try{

        await AuthService.studentLogin(this.state.username, this.state.password)
        .then(() =>
          {
            if (this.state.username == sessionStorage.getItem("user"))
            {
                      window.location = "/list-of-jobs"
            }
            console.log(sessionStorage.getItem("user"));
            /*const user = JSON.parse(sessionStorage.getItem("user"));
            console.log(sessionStorage.getItem("token"));*/

          },
            (error) =>
              {
                console.log(error);
              }
          );
        }
      catch(err)
          {
            console.log(err);
          }
      };

    getToken = () =>
    {
        //sessionStorage.getItem('token');
    }


    redirectToRegister = () =>
    {
        console.log("redirec to home")
    }


    render() {
     const{username, password} = this.state

      const InputContainer = (styled.div`
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        align-items: center;
        height: 80%;
        width: 100%;
      `);

      const ButtonContainer = (styled.div`
        margin: 1rem 0 2rem 0;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
      `);

      const ForgotPassword = (styled.h4`
        cursor: pointer;
      `);

      const HorizontalRule = (styled.hr`
        width: 90%;
        height: 0.3rem;
        border-radius: 0.8rem;
        border: none;
        background: linear-gradient(to right, #14163c 0%, #03217b 79%);
        background-color: #ebd0d0;
        margin: 1.5rem 0 1rem 0;
        backdrop-filter: blur(25px);
      `);

      return (
        <div>
          <h2 align="center"> Student Login!</h2>

            <form onSubmit = {this.submitHandler} align="center">
                <div>
                <input
                      type="text"
                      id='username'
                      placeholder = "Username"
                    //  autoComplete = "off"
                      name = "username"
                      value= {username}
                      //onChange={(e) => this.changeHandler}
                    //  onChange={(e) =>   this.setState({ username: e.target.value })}
                        onChange={this.changeHandler.bind(this)}
                      required
                      />
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <div>
                <input
                      type="password"
                      placeholder = "Password"
                      name = "password"
                      value={password}
                      onChange={this.changeHandler.bind(this)}
                      />
                </div>

          <ForgotPassword><Link to='/student-register'> Student Regitser?</Link></ForgotPassword>

          <div/>
            <StyledButton type = "submit" content = "Submit" onSubmit = {this.submitHandler}></StyledButton>
          </form>
          <ToastsContainer
            store={ToastsStore}
            position={ToastsContainerPosition.BOTTOM_CENTER}
          />

        </div>
 );

}
} export default StudentLogin;
