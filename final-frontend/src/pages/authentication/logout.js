//IMPORTING FUNCTIONALITIES
import React from 'react';
import AuthService from "../../services/auth.service";

function Logout()
  {
    AuthService.logoutStudent();
    sessionStorage.getItem("user");
    sessionStorage.getItem("userId");
    sessionStorage.getItem("job_id");
    sessionStorage.getItem("application_id");
    return(
        <div>
          <h2 align="center"> Goodbye</h2>
        </div>
 );

} export default Logout;
