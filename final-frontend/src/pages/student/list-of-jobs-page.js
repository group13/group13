//import dependancies
import React,{useEffect,useState} from 'react'
import axios from "axios";
//services to load
import JobService from "../../services/jobs.service"; //to get job data
//components to load
import LoadJobs from "../../components/student/load-jobs";
import Navbar from "../../components/student/common/NavBar";
//styles to load
import "../../styles/Job.scss";
import "../../styles/job-pres.scss";


function List_of_jobs()
{
  //get student details
  const student_id = sessionStorage.getItem("userId");
  const student = sessionStorage.getItem("user")
  const [jobData,setJob] = useState([]);
  React.useEffect(()=>{
    (async function(){
       var listOfJobs =  await JobService.getListOfJobs();
       setJob(listOfJobs)
       //console.log("Test (1) List of Jobs: ", jobData);
    })();
  },[])


  if (student_id == "null"){
    return (
      <div>
        <Navbar />
          <div className="header"></div>
          <h1> Please Log In!</h1> <br/>
      </div>
    );
  }

  else{
      return (
        <div>
          <Navbar />
            <div className = "heading-container"><br/>
                <h1> Welcome to HireIn {student}!</h1>
                  <h2> View our jobs below</h2>
            </div>
          <LoadJobs data={jobData}/>
        </div>
      );
    }
}
export default List_of_jobs;
