///import dependancies
import React,{useEffect,useState} from 'react'
import axios from "axios";
//services to load
import EmployerService from "../../services/employer.service"; //to get job data
//components to load
import Navbar from "../../components/employer/common/NavBar";
//styles to load
import "../../styles/Job.scss";
import "../../styles/student-styles.css";

import {DatePicker} from 'rsuite';
import 'rsuite/dist/rsuite.min.css';

function Application_status()
  {
    const posted_job_id = sessionStorage.getItem("employer_job_id");
    const employer_id  = sessionStorage.getItem("employerId");
    const employer = sessionStorage.getItem("employer");

    const [applicantData,setapplicantData] = useState({
      applicant: '',
      status: '',
    });

    React.useEffect(()=>{
      (async function(){
         var job_post_to_edit =  await EmployerService.getEmployerSpecificJob(posted_job_id);
         console.log(job_post_to_edit)
         setapplicantData({applicant: job_post_to_edit[0]})
         //console.log("Test (1) Job: ", applicantData.applicant);
      })();
    },[])

    console.log("Test (1) Job: ", applicantData.job);
    if (employer_id == "null"){
      return (
        <div>
          <Navbar />
            <div className="header"></div>
            <h1> Please Log In!</h1> <br/>
        </div>
      );
    }

    else{
      //CREATE THE JOB
      const submitHandler = async (e) =>
      {
        e.preventDefault();
        try{
          await EmployerService.editEmployerPostedJob()
          .then(() =>
            {
              console.log("Applied For Job Success");
            },
              (error) =>
                {
                  console.log(error);
                }
            );
          }
        catch(err)
            {
              console.log(err);
            }
        };

      //IF INTERESTS FIELD CHANFGES
      const handleChange = (event) => {
        	event.persist();
        	setapplicantData((applicantData) => ({
        		...applicantData,
        		status: event.target.value,
        	}));
      }

       return (
         <div>
           <Navbar />
           <br/>
             <h1>Applicant Status Job </h1>
             <div>
                 <p> <strong> Company Name: </strong> {applicantData.applicant.id}</p>
             </div><br/><br/>

             <form onSubmit={submitHandler}>
                 <div>
                   <textarea
                     id='job_tile'
                     placeholder = 'Application Status'
                     name = "job_tile"
                     value = {applicantData.status}
                     onChange={handleChange}
                     />
                  </div>


                 <br/><br/><br/>
                 <p> <button className = "button-9" type = "submit" content = "Submit" onSubmit = {submitHandler}> Submit: </button></p>
             </form>
           </div>
       );
}}export default Application_status;
