//import dependancies
import React,{useEffect,useState} from 'react'
import axios from "axios";
//services to load
import EmployerService from "../../services/employer.service"; //to get job data
//components to load
import LoadEmployerJobs from "../../components/employer/load-employer-jobs";
import Navbar from "../../components/employer/common/NavBar";
//styles to load
import "../../styles/Job.scss";


function List_of_employer_jobs()
{
  //get student details
  const employer_id  =  sessionStorage.getItem("employerId");
  const employer = sessionStorage.getItem("employer");

  const [employerJobData, setEmployerJob] = useState([]);
  React.useEffect(()=>{
    (async function(){
       var listOfEmployerJobs =  await EmployerService.getEmployerPostedJobs(employer_id);
       setEmployerJob(listOfEmployerJobs)
       //console.log("Test (1) List of Jobs: ", jobData);
    })();
  },[])


  if (employer_id == "null"){
    return (
      <div>
        <Navbar />
          <div className="header"></div>
          <h1> Please Log In!</h1> <br/>
      </div>
    );
  }

  else{
      return (
        <div>
          <Navbar />
            <div className="header"></div>
            <h1> Welcome {employer}, here are your posted jobs!</h1> <br/>
          <div>Jobs</div>
          <LoadEmployerJobs data={employerJobData}/>
        </div>
      );
    }
}
export default List_of_employer_jobs;
