///import dependancies
import React,{useEffect,useState} from 'react'
import axios from "axios";
//services to load
import EmployerService from "../../services/employer.service"; //to get job data
//components to load
import Navbar from "../../components/employer/common/NavBar";
//styles to load
import "../../styles/Job.scss";
import "../../styles/student-styles.css";

import {DatePicker} from 'rsuite';
import 'rsuite/dist/rsuite.min.css';

function Edit_posted_job()
  {
    const posted_job_id = sessionStorage.getItem("employer_job_id");
    const employer_id  = sessionStorage.getItem("employerId");
    const employer = sessionStorage.getItem("employer");

    const [jobPostData,setJobPostData] = useState({
      job: '',
      job_tile: '',
      description: '',
      company_name: '',
      salary: '',
      deadline: '',
      location: '',
      category: '',
    });

    React.useEffect(()=>{
      (async function(){
         var job_post_to_edit =  await EmployerService.getEmployerSpecificJob(posted_job_id);
         console.log(job_post_to_edit)
         setJobPostData({job: job_post_to_edit[0]})
         console.log("Test (1) Job: ", jobPostData.job);
      })();
    },[])

     //console.log("Test (1) Job: ", jobPostData.job);
    if (employer_id == "null"){
      return (
        <div>
          <Navbar />
            <div className="header"></div>
            <h1> Please Log In!</h1> <br/>
        </div>
      );
    }

    else{

      //CREATE THE JOB
      const submitHandler = async (e) =>
      {
        e.preventDefault();
        const job_tile = jobPostData.job_tile;
        const description = jobPostData.description;
        const company_name = jobPostData.company_name;
        const salary = jobPostData.salary;
        const deadline = jobPostData.deadline;

        try{
          await EmployerService.editEmployerPostedJob(
            job_tile,
            description,
            company_name,
            salary,
            deadline,
            employer_id
          )
          .then(() =>
            {
              console.log("Applied For Job Success");
            },
              (error) =>
                {
                  console.log(error);
                }
            );
          }
        catch(err)
            {
              console.log(err);
            }
        };



      //IF INTERESTS FIELD CHANFGES
      const handleJobTitleChange = (event) => {
        	event.persist();
        	setJobPostData((jobPostData) => ({
        		...jobPostData,
        		job_tile: event.target.value,
        	}));
        };

      const handleDescriptionChange = (event) => {
        	event.persist();
        	setJobPostData((jobPostData) => ({
        		...jobPostData,
        		description: event.target.value,
        	}));
      }

      const handleCompanyNameChange = (event) => {
          event.persist();
          setJobPostData((jobPostData) => ({
            ...jobPostData,
            company_name: event.target.value,
          }));
        }

      const handleSalaryChange = (event) => {
          event.persist();
          setJobPostData((jobPostData) => ({
            ...jobPostData,
          salary: event.target.value,
         }));
     }

     const handleCategoryChange = (event) => {
         event.persist();
         setJobPostData((jobPostData) => ({
           ...jobPostData,
         category: event.target.value,
        }));
    }

    const handleLocationChange = (event) => {
        event.persist();
        setJobPostData((jobPostData) => ({
          ...jobPostData,
        location: event.target.value,
       }));
   }

      const handleDeadlineChangeStr = (event) => {
          console.log(event.target.value)
          console.log(jobPostData.deadline)
          //event.persist();
          setJobPostData((jobPostData) => ({
            ...jobPostData,
          deadline: event.target.value,
        }));

     }

     const handleDeadlineChange = (range) => {
         var deadlineDate = new Intl.DateTimeFormat('en-GB', {year: 'numeric', month: '2-digit',day: '2-digit'}).format(range);
         console.log(deadlineDate);
         setJobPostData((jobPostData) => ({
           ...jobPostData,
         deadline: deadlineDate,
       }));
    }
       return (
         <div>
           <Navbar />
           <br/>
             <h1>Edit Job </h1>
             <div>
                 <p> <strong> Company Name: </strong> {jobPostData.job.company_name}</p>
                 <p> <strong> Description: </strong> {jobPostData.job.location}</p>
                 <p> <strong> Interest: </strong> {jobPostData.job.description}</p>
                 <p> <strong> Created At: </strong> {jobPostData.job.job_title}</p>
                 <p> <strong> Application Status: </strong> {jobPostData.job.deadline}</p>
             </div><br/><br/>

             <form onSubmit={submitHandler}>
                 <div>
                   <textarea
                     id='job_tile'
                     placeholder = {jobPostData.job.job_title}
                     name = "job_tile"
                     value = {jobPostData.job_tile}
                     onChange={handleJobTitleChange}
                     />
                  </div>


                 <div>
                   <textarea
                         type="textarea"
                         placeholder = "Description"
                         id = 'description'
                         name = "description"
                         value = {jobPostData.description}
                         onChange={handleDescriptionChange}
                         />
                 </div>

                  <div>
                      <textarea
                          placeholder = "Company Name"
                          id = 'company_name'
                          name = "company_name"
                          value = {jobPostData.company_name}
                          onChange={handleCompanyNameChange}
                       />
                </div>


                <div>
                    <textarea
                        placeholder = "Salary"
                        id = 'salary'
                        name = "salary"
                        value = {jobPostData.salary}
                        onChange={handleSalaryChange}
                     />
              </div>

              <div>
                  <textarea
                      placeholder = "Location"
                      id = 'location'
                      name = "location"
                      value = {jobPostData.location}
                      onChange={handleLocationChange}
                   />
            </div>


            <div>
                  <textarea
                      placeholder = "category"
                      id = 'category'
                      name = "category"
                      value = {jobPostData.category}
                      onChange={handleCategoryChange}
                  />
              </div>


               <div>
               <label> Enter Deadline </label><br/>
               <DatePicker
                   placeholder = "Deadline"
                   id = 'deadline'
                   valueName="selected"
                   format= 'dd/MM/yyyy'
                   onChange={handleDeadlineChange}
               />
               </div>

                 <br/><br/><br/>
                 <p> <button className = "button-9" type = "submit" content = "Submit" onSubmit = {submitHandler}> Submit: </button></p>
             </form>
           </div>
       );

  }
}export default Edit_posted_job;
