//import dependancies
import React,{useEffect,useState} from 'react'
import axios from "axios";
//services to load
import JobService from "../../services/jobs.service"; //to get job data
//components to load
import Navbar from "../../components/student/common/NavBar";
//styles to load
import "../../styles/Job.scss";
import "../../styles/student-styles.css";



function Apply_for_job()
  {
    const jobToApply =  sessionStorage.getItem("job_id");
    const student_id = sessionStorage.getItem("userId");
    const student = sessionStorage.getItem("user")
    console.log("job to apply for", jobToApply);
    const [newAppData,setNewAppData] = useState({
      job: '',
      interest: '',
      experience: '',
    });
    React.useEffect(()=>{
      (async function(){
          var jobDataOne =  await JobService.getSpecificJobs(jobToApply);
          setNewAppData({job: jobDataOne[0]})
      })();
    },[])

    if (student_id == "null"){
      return (
        <div>
          <Navbar />
            <div className="header"></div>
            <h1> Please Log In!</h1> <br/>
        </div>
      );
    }

    else{

  /* CONSOLE LOG FOR TESTING
      console.log("your job is", newAppData.job);
      console.log("your interest is", newAppData.interest);
      console.log("your experience is", newAppData.experience);
  */

      //PROCESS SUBMISSION OF APPLICATION
      const submitHandler = async (e) =>
      {
        e.preventDefault();
        const interest = newAppData.interest;
        const experience = newAppData.experience

  /*  //  console.log("your job Id is", data1.job.id);
      //  console.log("your employer Id is", data1.job.employer);
      //  console.log("your student Id is", localStorage.getItem("userId"));
      //  console.log("created by", localStorage.getItem("userId"));
      //  console.log("your interest is", interest);
      //  console.log("your experience is", experience);
      //  console.log("app status") */

        try{
          await JobService.applyForJob(
            interest, //user entered field
            experience, //user entered field
            student_id, //student id of this sessoin
            newAppData.job.employer,
            newAppData.job.id,
            student_id //created by
          )
          .then(() =>
            {
              console.log("Applied For Job Success");
            },
              (error) =>
                {
                  console.log(error);
                }
            );
          }
        catch(err)
            {
              console.log(err);
            }
        };

      //IF INTERESTS FIELD CHANFGES
      const handleInterestChange = (event) => {
        	event.persist();
        	setNewAppData((newAppData) => ({
        		...newAppData,
        		interest: event.target.value,
        	}));
        };

        const handleExperienceChange = (event) => {
        	event.persist();
        	setNewAppData((newAppData) => ({
        		...newAppData,
        		experience: event.target.value,
        	}));
        }

       return (
         <div>
           <Navbar />
           <br/>

             <div className="heading-container">   <h1>Apply for Job</h1> </div>

             <div className= "job-container">
                 <p> <strong> Company Name: </strong> {newAppData.job.company_name}</p>
                 <p> <strong> Description: </strong> {newAppData.job.description}</p>
                 <p> <strong> Job Title: </strong> {newAppData.job.job_title}</p>
                 <p> <strong> Location: </strong> {newAppData.job.location}</p>
                 <p> <strong> Deadline: </strong> {newAppData.job.deadline}</p>
                 <p> <strong> Employer ID: </strong> {newAppData.job.employer}</p>
             <br/>
            </div><br/><br/>

             <h2>Send Application</h2>
             <br/>

             <form onSubmit={submitHandler}>
                 <div>
                   <input
                   type="textarea"
                   id='interest'
                   placeholder = "Please Enter Why this Role Is of Interest to you"
                   name = "interest"
                   value = {newAppData.interest}
                   onChange={handleInterestChange} />
                 </div>


                 <div>
                   <input
                         type="textarea"
                         placeholder = "Please Enter your previous Work Experience"
                         id = 'experience'
                         name = "experience"
                         value = {newAppData.experience}
                         onChange={handleExperienceChange}
                         />
                 </div>
                 <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/> <br/><br/><br/><br/><br/><br/>

                 <p> <button className = "button-9" type = "submit" content = "Submit" onSubmit = {submitHandler}> Submit: </button></p>

             </form>

           </div>
       );

  }
}export default Apply_for_job;
