//import dependancies
import React,{useEffect,useState} from 'react'
import axios from "axios";
//services to load
import ApplicationService from "../../services/application.service"; //to get job data
//components to load
import Navbar from "../../components/student/common/NavBar";
//styles to load
import "../../styles/Job.scss";



function Edit_applications()
{
//  console.log("application id: ", sessionStorage.getItem("application_id"));
  const appl_id = sessionStorage.getItem("application_id")
  const student_id = sessionStorage.getItem("userId");
  const student = sessionStorage.getItem("user")
  //console.log(appl_id);

  const [applData,setApplData] = useState({
    job: '',
    interest: '',
    experience: '',
    selected: '',
    field: '',
  });

  React.useEffect(()=>{
    (async function(){
       var application =  await ApplicationService.viewStudentApplication(sessionStorage.getItem("application_id"));
       console.log(application)
       setApplData({job: application[0]})
       console.log("Test (1) Application: ", applData.job);
    })();
  },[])

  console.log("value to assess is", applData.job)
  if (applData.job == null)
  {
    console.log("aborting");
    return (
      <div>
        <Navbar />
          <div className="header"></div>
          <h1> Welcome to hireIn {sessionStorage.getItem("user")} !</h1> <br/>
          <p> Error </p>
      </div>
      );
  }

  else
  {
      const selectChangeHandler = (event) => {
          event.persist();
          setApplData((applData) => ({
            ...applData,
            selected: event.target.value,
          }));
           //console.log(applData.selected);
        };

        const handleChange = (event) => {
          event.persist();
          setApplData((applData) => ({
            ...applData,
            field: event.target.value,
          }));
          //console.log(applData.field);
        }

        const deleteHandler = (e) => {
          e.persist();
          e.preventDefault();
          ApplicationService.deleteStudentApplication(appl_id)
        }

        const submitHandler = (e) => {
          e.persist();
          e.preventDefault();

          if ((applData.selected == 'interest') && (applData.field != ''))
          {
            /* //------CONSOLE LOG FOR TESTS -------//
            console.log("id is", appl_id.job.id)
            console.log("employer is", appl_id.job.employer)
            console.log("created by", appl_id.job.created_by)
            console.log("experience", appl_id.job.experience)
            console.log("your selected field is", appl_id.selected);
            console.log("your selected data is", appl_id.field);
            */
            var interest =  applData.field;

            ApplicationService.editStudentApplication(
                  appl_id,
                  applData.job.job,
                  applData.job.employer,
                  applData.job.created_by,
                  applData.job.experience,
                  interest,
              );
          }

         else if ((applData.selected == 'experience') && (applData.field != ''))
          {
            /* //------CONSOLE LOG FOR TESTS -------//
            console.log("id is", data3.job.id)
            console.log("employer is", data3.job.employer)
            console.log("created by", data3.job.created_by)
            console.log("experience", data3.job.experience)
            console.log("your selected field is", data3.selected);
            console.log("your selected data is", data3.field);
            */
            var experience =  appl_id.field;
            ApplicationService.editStudentApplication(
                  appl_id,
                  applData.job.job, //need to find
                  applData.job.employer,
                  applData.job.created_by,
                  experience,
                  applData.job.interest,
              );
            }

          else{
             console.log("invalid data entered")
          }
        }

      return (
        <div>
          <Navbar />
            <div className="header"></div>
            <h1> Welcome to hireIn {sessionStorage.getItem("user")} !</h1> <br/>
          <div>
              <p> <strong> Company Name: </strong> {applData.job.job}</p>
              <p> <strong> Description: </strong> {applData.job.app_status}</p>
              <p> <strong> Interest: </strong> {applData.job.interest}</p>
              <p> <strong> Created At: </strong> {applData.job.created_at}</p>
              <p> <strong> Application Status: </strong> {applData.job.app_status}</p>

          </div>


          <br/><br/>
          <form onSubmit={submitHandler}>
          <div>
          <select
            value={applData.selected}
            onChange={selectChangeHandler}
            >
              <option value="interest">interests</option>
              <option value="experience">experince</option>
          </select>     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <button type = "submit" content = "Submit" onSubmit = {submitHandler}> Change Inputsss</button>
          <button onClick={deleteHandler}> Delete Application </button>
          </div>

          <br/><br/>

          <div>
          <textarea
           type="textarea"
           id = 'input'
           name = "input"
           value = {applData.experience}
           onChange={handleChange}> Enter Change Here:
          </textarea>
          </div>
          </form>
        </div>
      );
  }
}
export default Edit_applications;
