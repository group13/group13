//import dependancies
import React,{useEffect,useState} from 'react';
import axios from "axios";
//services to load
import ApplicationService from "../../services/application.service"; //to get applicatio  data
//components to load
import LoadApplications from "../../components/student/load-applications";
import Navbar from "../../components/student/common/NavBar";
//styles to load
import "../../styles/Job.scss";


function Student_applications()
{
  //get student details
  const student_id = sessionStorage.getItem("userId");
  const student = sessionStorage.getItem("user");
  const [appData, setAppData] = useState([]);

  //get students application
  React.useEffect(()=>{
    (async function(){
       var application =  await ApplicationService.getStudentApplication(student_id);
       setAppData(application)
       console.log("Test (1) List of Jobs: ", appData);
    })();
  },[])

  if (student_id == "null"){
    return (
      <div>
        <Navbar />
          <div className="header"></div>
          <h1> Please Log In!</h1> <br/>
      </div>
    );
  }

  else{

     //render applications
      return (
        <div>
          <Navbar />
            <div className="heading-container">
            <h1> Here are your applications {student}!</h1> <br/>
          </div>
          <LoadApplications data={appData}/>
        </div>
      );
    }
  }
export default Student_applications;
