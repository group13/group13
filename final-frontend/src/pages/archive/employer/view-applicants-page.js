//import dependancies
import React,{useEffect,useState} from 'react'
import axios from "axios";
//services to load
import EmployerService from "../../services/employer.service"; //to get job data
//components to load
import LoadApplicants from "../../components/employer/load-applicants-to-job";
import Navbar from "../../components/employer/common/NavBar";
//styles to load
import "../../styles/Job.scss";


function List_of_applicant_for_job()
{
  //get student details
  const employer_id  = sessionStorage.getItem("employerId");
  const employer = sessionStorage.getItem("employer");
  const posted_job_id = sessionStorage.getItem("employer_job_id");

  console.log("job id", posted_job_id)

  const [applJobData, setApplJobData] = useState([]);
  React.useEffect(()=>{
    (async function(){
       var listOfApplicants =  await EmployerService.getApplicantsToEmployerJob(employer_id);
       setApplJobData(listOfApplicants)
       //console.log("Test (1) List of Jobs: ", jobData);
    })();
  },[])


  if (employer_id == "null"){
    return (
      <div>
        <Navbar />
          <div className="header"></div>
          <h1> Please Log In!</h1> <br/>
      </div>
    );
  }

  else{
      return (
        <div>
          <Navbar />
            <div className="header"></div>
            <h1> View Your applicants</h1> <br/>
          <div>Applicants</div>
          <LoadApplicants data={applJobData} jobIdFilter = {posted_job_id} />
        </div>
      );
    }
}
export default List_of_applicant_for_job;
