///import dependancies
import React,{useEffect,useState} from 'react'
import axios from "axios";
//services to load
import EmployerService from "../../services/employer.service"; //to get job data
//components to load
import Navbar from "../../components/employer/common/NavBar";
//styles to load
import "../../styles/Job.scss";
import "../../styles/student-styles.css";

import {DatePicker} from 'rsuite';
import 'rsuite/dist/rsuite.min.css';

function Create_job()
  {
    const employer_id  = sessionStorage.getItem("employerId");
    const employer = sessionStorage.getItem("employer");

    const [newJobData,setNewJobData] = useState({
      job_tile: '',
      description: '',
      company_name: '',
      salary: '',
      deadline: '',
      location: '',
      category: '',
    });

    if (employer_id == "null"){
      return (
        <div>
          <Navbar />
            <div className="header"></div>
            <h1> Please Log In!</h1> <br/>
        </div>
      );
    }

    else{

      //CREATE THE JOB
      const submitHandler = async (e) =>
      {
        e.preventDefault();
        const job_tile = newJobData.job_tile;
        const description = newJobData.description;
        const company_name = newJobData.company_name;
        const salary = newJobData.salary;
        const deadline = newJobData.deadline;

        try{
          await EmployerService.createNewJob(
            job_tile,
            description,
            company_name,
            salary,
            deadline,
            employer_id
          )
          .then(() =>
            {
              console.log("Applied For Job Success");
            },
              (error) =>
                {
                  console.log(error);
                }
            );
          }
        catch(err)
            {
              console.log(err);
            }
        };



      //IF INTERESTS FIELD CHANFGES
      const handleJobTitleChange = (event) => {
        	event.persist();
        	setNewJobData((newJobData) => ({
        		...newJobData,
        		job_tile: event.target.value,
        	}));
        };

      const handleDescriptionChange = (event) => {
        	event.persist();
        	setNewJobData((newJobData) => ({
        		...newJobData,
        		description: event.target.value,
        	}));
      }

      const handleCompanyNameChange = (event) => {
          event.persist();
          setNewJobData((newJobData) => ({
            ...newJobData,
            company_name: event.target.value,
          }));
        }

      const handleSalaryChange = (event) => {
          event.persist();
          setNewJobData((newJobData) => ({
            ...newJobData,
          salary: event.target.value,
         }));
     }

     const handleCategoryChange = (event) => {
         event.persist();
         setNewJobData((newJobData) => ({
           ...newJobData,
         category: event.target.value,
        }));
    }

    const handleLocationChange = (event) => {
        event.persist();
        setNewJobData((newJobData) => ({
          ...newJobData,
        location: event.target.value,
       }));
   }

      const handleDeadlineChangeStr = (event) => {
          console.log(event.target.value)
          console.log(newJobData.deadline)
          //event.persist();
          setNewJobData((newJobData) => ({
            ...newJobData,
          deadline: event.target.value,
        }));

     }

     const handleDeadlineChange = (range) => {
         var deadlineDate = new Intl.DateTimeFormat('en-GB', {year: 'numeric', month: '2-digit',day: '2-digit'}).format(range);
         console.log(deadlineDate);
         setNewJobData((newJobData) => ({
           ...newJobData,
         deadline: deadlineDate,
       }));
    }
       return (
         <div>
           <Navbar />
           <br/>
             <h1>Create New Job </h1>
             <form onSubmit={submitHandler}>
                 <div>
                   <textarea
                     id='job_tile'
                     placeholder = "Job Title"
                     name = "job_tile"
                     value = {newJobData.job_tile}
                     onChange={handleJobTitleChange}
                     />
                  </div>


                 <div>
                   <textarea
                         type="textarea"
                         placeholder = "Description"
                         id = 'description'
                         name = "description"
                         value = {newJobData.description}
                         onChange={handleDescriptionChange}
                         />
                 </div>

                  <div>
                      <textarea
                          placeholder = "Company Name"
                          id = 'company_name'
                          name = "company_name"
                          value = {newJobData.company_name}
                          onChange={handleCompanyNameChange}
                       />
                </div>


                <div>
                    <textarea
                        placeholder = "Salary"
                        id = 'salary'
                        name = "salary"
                        value = {newJobData.salary}
                        onChange={handleSalaryChange}
                     />
              </div>

              <div>
                  <textarea
                      placeholder = "Location"
                      id = 'location'
                      name = "location"
                      value = {newJobData.location}
                      onChange={handleLocationChange}
                   />
            </div>


            <div>
                  <textarea
                      placeholder = "category"
                      id = 'category'
                      name = "category"
                      value = {newJobData.category}
                      onChange={handleCategoryChange}
                  />
              </div>


               <div>
               <label> Enter Deadline </label><br/>
               <DatePicker
                   placeholder = "Deadline"
                   id = 'deadline'
                   valueName="selected"
                   format= 'dd/MM/yyyy'
                   onChange={handleDeadlineChange}
               />
               </div>

                 <br/><br/><br/>
                 <p> <button className = "button-9" type = "submit" content = "Submit" onSubmit = {submitHandler}> Submit: </button></p>
             </form>
           </div>
       );

  }
}export default Create_job;
