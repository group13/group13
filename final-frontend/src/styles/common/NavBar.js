import React, { useState } from "react";
import "./navbar.css";
import "./navbar.scss";

function Navbar() {
  const [active, setActive] = useState("nav__menu");
  const [icon, setIcon] = useState("nav__toggler");
  const navToggle = () => {
    if (active === "nav__menu") {
      setActive("nav__menu nav__active");
    } else setActive("nav__menu");

    // Icon Toggler
    if (icon === "nav__toggler") {
      setIcon("nav__toggler toggle");
    } else setIcon("nav__toggler");
  };

  const student_id = sessionStorage.getItem("userId");
  if (student_id == "null"){
      return (
        <nav className="nav">
        <ul className={active}>
        <button className="button-47">
          <a href="/" className="nav__link">
            login
          </a>
        </button>
      </ul>
      <div onClick={navToggle} className={icon}>
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
    </nav>
    )
  }
else{
    return (
      <nav className="nav">
        <a href="#" className="nav__brand">
         HIREIN
        </a>

        <ul className={active}>
          <li className="nav__item">
            <a href="#" className="nav__link">
              Home
            </a>
          </li>
          <li className="nav__item">
            <a href="/list-of-jobs" className="nav__link">
              View Jobs
            </a>

          </li>
          <li className="nav__item">
            <a href="/my-applications" className="nav__link">
              View Applications
            </a>
          </li>

          <button className="button-47">
            <a href="/logout" className="nav__link">
              Logout
            </a>
          </button>
        </ul>
        <div onClick={navToggle} className={icon}>
          <div className="line1"></div>
          <div className="line2"></div>
          <div className="line3"></div>
        </div>
      </nav>
    );
  }
}

export default Navbar;

/* import { useState } from "react";
import {Link,NavLink} from "react-router-dom";
import './navbar.scss';
import menuIcon from "./menu.png"
import newMovers from "./menu.png"

function Navbar() {
  const [menuActive, setMenuActive] = useState(false);

    const showMenu = ()=>{
        setMenuActive(!menuActive);
    }

  return (
    <nav className="navbar">
     <div className="container navbar__container">
       <Link to='/employerhome'className='navbar__logo'> <img width={15} src={newMovers}></img></Link>
       <button onClick={showMenu}  className='navbar__toggle'>
       <img src={menuIcon} alt="movers"></img>
       </button>
       <div className={menuActive ? "navbar__menu navbar__menu--show" : "navbar__menu"}>
         <NavLink onClick={showMenu} to="/jobs" className='navbar__menu-link'>View Jobs</NavLink>
         <NavLink onClick={showMenu} to="/jobs" className='navbar__menu-link'>View Applications</NavLink>
         <NavLink onClick={showMenu} to="/jobs"  className='navbar__menu-link'>Settings</NavLink>
         <NavLink onClick={showMenu} to="/"  className='navbar__menu-link'>Logout </NavLink>
       </div>
     </div>
    </nav>
  );
}

export default Navbar;
*/
