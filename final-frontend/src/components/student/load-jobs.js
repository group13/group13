import React, { useEffect, useState } from "react";
import {Link} from 'react-router-dom';
//import OneJob from "./archive/OneJob";
import "../../styles/job-pres.scss";

const LoadJobs = ({ data }) =>
{
  const student_id = sessionStorage.getItem("userId");
  const student = sessionStorage.getItem("user");
  if (student == "null")
  {
    return (
      <div className="jobs">
       <label> Please Login </label>
      </div>
    );
  }

  else
  {
    let processApplication = (id) =>
    {
      sessionStorage.setItem("job_id", id);
      console.log(" id: ", id);
      window.location = "/apply-for-job";
    };

    return (
      <div className="jobs">
      <br/>
      {data.map(({ id, job_title, company_name, description,  location, deadline, job_category, salary }) => (
        <p key={id}>

          <div className= "job-container">
            <div className="part1">
                <div className="company">
                  <span className="cname">{job_title}</span>
                </div>
            </div>
          </div>

          <div className= "job-container">
              <strong>Company: </strong> {company_name}'s <br/>
              <strong>Description: </strong>{description}  <br/>
              <strong>Location:  </strong> {location}  <br/>

              <strong>Category:  </strong> {job_category}<br/>
              <strong>Salary:  </strong> {salary}<br/>
              <strong>Deadline:  </strong> {deadline}<br/>
              <text>
                <span class="date">
                      <span class="month"></span><br/>
                      <span class="day">{deadline}</span>
                </span>
              </text>

              <button className = "button-9"
                      onClick={() => {processApplication(parseInt(id));}}>
                      Apply
              </button> <br/><br/><br/>
          </div><br/> <br/><br/>
        </p>

      ))}

      </div>
    );

}};

export default LoadJobs;
