import React, { useEffect, useState } from "react";
import {Link} from 'react-router-dom';
//import OneJob from "./archive/OneJob";
import "../../styles/job-pres.scss";


const LoadApplications = ({ data }) =>
{
  const student_id = sessionStorage.getItem("userId");
  const student = sessionStorage.getItem("user")

  let editApplication = (id) =>
  {
    sessionStorage.setItem("application_id", id);
    window.location = "/edit-application";
  };

  return (
    <div className="jobs">
    <br/>
    {data.map(({ id, app_status, interest, experience,  created_at, job}) => (
      <p key={id}>
          <div className= "job-container">
            <div className="part1">
                <div className="company">
                  <span className="cname">{created_at}</span>
                </div>
            </div>
          </div>
        <div className= "job-container">
              <strong>id: </strong> {id} <br/>
              <strong>Status: </strong> {app_status}'s <br/>
              <strong>Interest: </strong> {interest}'s <br/>
              <strong>Experience: </strong>{experience}  <br/>
              <strong>Applied At:  </strong> {created_at}<br/>
              <strong>Job Id:  </strong> {job}<br/>
              <button className = "button-9"
                  onClick={() => {editApplication(parseInt(id));}}>
                  Edit Application
              </button> <br/><br/><br/>
            </div><br/><br/><br/>
              </p>
    ))}
    </div>
  );

};

export default LoadApplications;
