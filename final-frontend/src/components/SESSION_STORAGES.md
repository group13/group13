
SESSION STORAGES Student fields
----------------------

sessionStorage.getItem("job_id"); - job Id being applied to
sessionStorage.getItem("userId"); - student Id of session
sessionStorage.getItem("user"); - student name of session
sessionStorage.getItem("application_id") - application being processed


SESSION STORAGES Employer fields
----------------------
sessionStorage.getItem("employerId"); -employer id
sessionStorage.getItem("employer"); - employer Username
sessionStorage.getItem("employer_job_id"); - employer job being processed
