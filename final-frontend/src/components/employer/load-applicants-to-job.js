import React, { useEffect, useState } from "react";
import {Link} from 'react-router-dom';


const LoadApplications = ({ data, jobIdFilter}) =>
{

  console.log("Test (2): ", data);
  const posted_job_id = sessionStorage.getItem("employer_job_id");
  var filteredArray = data.filter(data => data.job ==  posted_job_id);
  let updateApplication = (id) =>
  {
    sessionStorage.setItem("application_id", id);
    console.log("application status");
    window.location = "/update-applicant-status";
  };

  return (
    <div className="jobs">
    <br/>
    {filteredArray.map(({ id, app_status, interest, experience,  created_at, job}) => (
      <p key={id}>
        <div className= "job-container">
              <strong>id: </strong> {id} <br/>
              <strong>Status: </strong> {app_status}'s <br/>
              <strong>Interest: </strong> {interest}'s <br/>
              <strong>Experience: </strong>{experience}  <br/>
              <strong>Applied At:  </strong> {created_at}<br/>
              <strong>Job Id:  </strong> {job}<br/>
              <button className = "button-9"
                  onClick={() => {updateApplication(parseInt(id));}}>
                  Update Application Status
              </button> <br/><br/><br/>
              </div>
              </p>
    ))}
    </div>
  );
};

export default LoadApplications;
