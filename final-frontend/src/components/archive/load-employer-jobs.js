import React, { useEffect, useState } from "react";
import {Link} from 'react-router-dom';
//import OneJob from "./archive/OneJob";


const Load_employer_jobs = ({ data }) =>
{
  let viewApplicants = (id) =>
  {
    console.log("success")
    sessionStorage.setItem("employer_job_id", id);
    window.location = "/view-applicants";
  };
  let editJobPost = (id) =>
  {
    console.log("success")
    sessionStorage.setItem("employer_job_id", id);
    //console.log(" id: ", id);
    window.location = "/edit-posted-jobs";
  };

  return (
    <div className="jobs">
    <br/>
    {data.map(({ id, job_title, company_name, description,  location, deadline, job_category, salary }) => (
      <p key={id}>

        <div className= "job-container">
          <div className="part1">
              <div className="company">
                <span className="cname">{job_title}</span>
              </div>
          </div>
        </div>

        <div className= "job-container">
            <strong>Company: </strong> {company_name}'s <br/>
            <strong>Description: </strong>{description}  <br/>
            <strong>Location:  </strong> {location}  <br/>

            <strong>Category:  </strong> {job_category}<br/>
            <strong>Salary:  </strong> {salary}<br/>
            <strong>Deadline:  </strong> {deadline}<br/>
            <text>
              <span class="date">
                    <span class="month"></span><br/>
                    <span class="day">{deadline}</span>
              </span>
            </text>

            <br/>  <br/><br/>
            <button className = "button-9"
                    onClick={() => {viewApplicants(parseInt(id));}}>
                    View Applicants
            </button>  <br/>

            <button className = "button-9"
                    onClick={() => {editJobPost(parseInt(id));}}>
                    Edit Post
            </button> <br/>

            <br/><br/>


        </div><br/>
      </p>
    ))}
    </div>
  );
};

export default Load_employer_jobs;
